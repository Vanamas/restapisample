# Rest API Sample in Kotlin

This repository contains a sample project demonstrating the usage of REST APIs with Kotlin. The project uses a number of modern Android technologies including Jetpack Compose for the UI, Retrofit for API calls, Room for local data persistence, Koin for dependency injection, Coroutines for asynchronous tasks, Lifecycle for managing component lifecycles, and Timber for logging.

## Technologies Used

- [Jetpack Compose](https://developer.android.com/jetpack/compose) - A modern, declarative UI toolkit for Android.
- [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.
- [Room](https://developer.android.com/jetpack/androidx/releases/room) - A persistence library that provides an abstraction layer over SQLite.
- [Koin](https://insert-koin.io/) - A pragmatic lightweight dependency injection framework for Kotlin.
- [Coroutines](https://kotlinlang.org/docs/coroutines-overview.html) - A Kotlin library for managing asynchronous tasks and more.
- [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle) - A collection of libraries that help you design robust, testable, and maintainable apps.
- [Timber](https://github.com/JakeWharton/timber) - A logger with a small, extensible API which provides utility on top of Android's normal Log class.

## Testing Libraries Used

- [Kotest](https://kotest.io/) - A flexible and comprehensive testing tool for Kotlin, packed with powerful capabilities.
- [MockK](https://mockk.io/) - A Kotlin-focused mocking library.

## Running the Project

To run the project, you will need Android Studio 2023.1.1 or later. Open the project in Android Studio and click `Run`. The project will be installed on the selected device or emulator.

## Contributing

Contributions are welcome. Please open an issue to discuss your ideas or submit a pull request.