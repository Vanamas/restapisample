package cz.vanama.restapisample.di

import cz.vanama.restapisample.data.common.ResponseHandler
import cz.vanama.restapisample.data.repository.PostRepositoryImpl
import cz.vanama.restapisample.data.usecase.CacheTimeExpiredUseCase
import cz.vanama.restapisample.data.usecase.IsDeviceOnlineUseCase
import cz.vanama.restapisample.domain.repository.PostRepository
import cz.vanama.restapisample.domain.usecase.GetPostUseCase
import cz.vanama.restapisample.domain.usecase.GetPostsUseCase
import cz.vanama.restapisample.ui.screen.post.PostViewModel
import cz.vanama.restapisample.ui.screen.post.PostsViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val useCasesModule = module {
    singleOf(::IsDeviceOnlineUseCase)
    singleOf(::CacheTimeExpiredUseCase)
    singleOf(::GetPostsUseCase)
    singleOf(::GetPostUseCase)
}

val repositoriesModule = module {
    singleOf(::ResponseHandler)
    singleOf(::PostRepositoryImpl) { bind<PostRepository>() }
}

val viewModelsModule = module {
    viewModelOf(::PostsViewModel)
    viewModelOf(::PostViewModel)
}
