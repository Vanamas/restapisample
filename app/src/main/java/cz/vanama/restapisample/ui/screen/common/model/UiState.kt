package cz.vanama.restapisample.ui.screen.common.model

import androidx.annotation.StringRes
import cz.vanama.restapisample.R
import cz.vanama.restapisample.domain.common.ErrorType
import cz.vanama.restapisample.domain.common.Result
import cz.vanama.restapisample.ui.screen.common.model.UiState.Error
import cz.vanama.restapisample.ui.screen.common.model.UiState.Loading
import cz.vanama.restapisample.ui.screen.common.model.UiState.Success

/**
 * A sealed interface that represents different states of the UI.
 *
 * @param T The type of data which is delivered in the [Success] state.
 * @see Loading
 * @see Success
 * @see Error
 */
sealed interface UiState<out T> {
    /**
     * Represents the loading state, indicating that some operation is in progress.
     */
    object Loading : UiState<Nothing>

    /**
     * Represents the success state, delivering the resulting data [T].
     *
     * @property data The data of type [T] resulting from the successful operation.
     */
    data class Success<out T>(val data: T) : UiState<T>

    /**
     * Represents the error state, delivering an error message and a retry action.
     *
     * @property errorMessage The string resource ID of the error message to be displayed.
     * @property retryAction A function to be invoked when retry action is performed.
     */
    data class Error(@StringRes val errorMessage: Int, val retryAction: () -> Unit) :
        UiState<Nothing>
}

/**
 * An extension function on [Result] that converts it to a corresponding [UiState].
 *
 * @param retryAction A function to be invoked when a retry action is needed.
 * @param errorMessage An optional custom error message string resource ID to be used in the error state.
 * If not provided, a default message will be selected based on the [Result.Error.errorType].
 * @return A corresponding [UiState] for the given [Result].
 */
fun <T> Result<T>.toUiState(
    retryAction: () -> Unit,
    @StringRes errorMessage: Int? = null
): UiState<T> {
    return when (this) {
        is Result.Success -> UiState.Success(data)
        is Result.Error -> UiState.Error(
            errorMessage = errorMessage ?: when (errorType) {
                ErrorType.NOT_CONNECTED -> R.string.error_not_connected
                ErrorType.RESPONSE_INTERNAL_SERVER_ERROR -> R.string.error_internal_server_error
                ErrorType.RESPONSE_NOT_FOUND -> R.string.error_content_not_found
                ErrorType.RESPONSE_TIMEOUT -> R.string.error_response_timeout
                ErrorType.RESPONSE_UNAUTHORIZED -> R.string.error_unauthorized
                else -> R.string.error_general
            },
            retryAction = retryAction
        )

        Result.Loading -> UiState.Loading
    }
}