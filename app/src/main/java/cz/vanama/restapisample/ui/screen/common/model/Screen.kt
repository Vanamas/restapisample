package cz.vanama.restapisample.ui.screen.common.model

/**
 * A base class for defining different screens in the application. It provides a mechanism for
 * managing navigation routes and their parameters.
 *
 * All concrete screens are represented by objects inheriting from this sealed class.
 * Each screen object defines a navigation route, which may contain placeholders for arguments.
 *
 * @property route The navigation route of this screen. May contain placeholders for arguments.
 *
 * @author Martin Vana
 */
sealed class Screen(
    val route: String
) {

    /**
     * Replaces placeholders in the [route] of this screen with actual values provided by the [arguments] map.
     *
     * For example, if the route is "post/{id}", calling this method with arguments map containing
     * an entry with key "id" and value "123" would result in a route "post/123".
     *
     * @param arguments A map of argument names to their actual values.
     *                  Keys should correspond to placeholders in the route (without braces).
     *
     * @return The updated route with placeholders replaced by actual argument values.
     */
    open fun withArguments(arguments: Map<String, String>): String {
        var updatedRoute = route
        for ((key, value) in arguments) {
            updatedRoute = updatedRoute.replace("{$key}", value)
        }
        return updatedRoute
    }

    /**
     * Represents the "Posts" screen in the application.
     */
    object PostsScreen : Screen("posts")

    /**
     * Represents the "Post Detail" screen in the application.
     *
     * This screen expects a route argument "id" representing the post's ID.
     */
    object PostDetailScreen : Screen("post/{id}") {
        const val ID = "id"
    }
}
