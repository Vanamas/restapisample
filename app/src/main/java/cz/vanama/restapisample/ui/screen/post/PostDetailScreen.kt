package cz.vanama.restapisample.ui.screen.post

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults.topAppBarColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextOverflow
import cz.vanama.restapisample.R
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.ui.screen.common.UiStateScreen
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

/**
 * Post detail screen.
 *
 * @author Martin Vana
 */
@Composable
fun PostDetailScreen(
    id: Long,
    onNavigateBack: () -> Unit,
    viewModel: PostViewModel = getViewModel { parametersOf(id) }
) {
    val state = viewModel.state.collectAsState()

    UiStateScreen(uiState = state.value) {
        Post(it, onNavigateBack)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun Post(post: Post, onNavigateBack: () -> Unit) {
    Scaffold(topBar = {
        TopAppBar(title = {
            Text(
                text = post.title,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
            navigationIcon = {
                IconButton(onClick = onNavigateBack) {
                    Icon(Icons.Filled.ArrowBack, null)
                }
            }, actions = {
                IconButton(onClick = {/* Do Something*/ }) {
                    Icon(Icons.Filled.Share, null)
                }
            },
            colors = topAppBarColors()
        )
    }) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
                .padding(dimensionResource(R.dimen.padding_base))
        ) {
            post.body?.let { text -> Text(text = text) }
        }
    }
}