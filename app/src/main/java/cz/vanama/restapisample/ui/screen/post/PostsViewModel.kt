package cz.vanama.restapisample.ui.screen.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.domain.usecase.GetPostsUseCase
import cz.vanama.restapisample.ui.screen.common.model.UiState
import cz.vanama.restapisample.ui.screen.common.model.toUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

/**
 * Posts view model.
 *
 * @author Martin Vana
 */
class PostsViewModel(private val getPostsUseCase: GetPostsUseCase) : ViewModel() {

    private val postsFlow = MutableStateFlow<UiState<List<Post>>>(UiState.Loading)
    val postsStateFlow = postsFlow.asStateFlow()

    init {
        reload()
    }

    fun reload(forceReload: Boolean = false) {
        viewModelScope.launch {
            postsFlow.value =
                getPostsUseCase(GetPostsUseCase.Params(forceReload)).toUiState({ reload() })
        }
    }
}