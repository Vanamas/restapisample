package cz.vanama.restapisample.ui.screen.common

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import cz.vanama.restapisample.R
import cz.vanama.restapisample.ui.theme.RestApiSampleTheme

/**
 * A [Composable] function that displays an error screen with a message and a retry button.
 *
 * The error message is centered on the screen and the retry button is placed below it.
 * The error message and the retry button are separated by a spacer of a base padding size.
 *
 * The error message is passed as a string resource ID, which is resolved to a string using [stringResource].
 * When the retry button is clicked, it executes the [onRetry] lambda.
 *
 * @param errorMessage The string resource ID of the error message to display.
 * @param onRetry A lambda that is executed when the retry button is clicked.
 */
@Composable
fun ErrorScreen(@StringRes errorMessage: Int, onRetry: () -> Unit) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxSize()
            .padding(dimensionResource(R.dimen.padding_base)),
    ) {
        Text(
            text = stringResource(errorMessage),
            color = MaterialTheme.colorScheme.primary,
            fontSize = MaterialTheme.typography.headlineMedium.fontSize,
            modifier = Modifier.padding(horizontal = dimensionResource(R.dimen.padding_base))
        )

        Spacer(modifier = Modifier.height(dimensionResource(R.dimen.padding_base)))

        Button(onClick = onRetry) {
            Text(text = stringResource(R.string.try_again))
        }
    }
}

@Preview
@Composable
private fun Preview() {
    RestApiSampleTheme {
        ErrorScreen(
            errorMessage = R.string.error_general,
            onRetry = {}
        )
    }
}