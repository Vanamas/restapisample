package cz.vanama.restapisample.ui.screen.common

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import cz.vanama.restapisample.ui.screen.common.model.Screen
import cz.vanama.restapisample.ui.screen.post.PostDetailScreen
import cz.vanama.restapisample.ui.screen.post.PostsScreen
import cz.vanama.restapisample.ui.theme.RestApiSampleTheme

/**
 * Main screen with navigation.
 *
 * @author Martin Vana
 */
@Composable
fun MainScreen() {
    val navController = rememberNavController()

    RestApiSampleTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            NavHost(navController = navController)
        }
    }
}

@Composable
internal fun NavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    Scaffold(
        modifier = modifier,
    ) { padding ->
        androidx.navigation.compose.NavHost(
            navController = navController,
            startDestination = Screen.PostsScreen.route,
            modifier = Modifier.padding(padding)
        ) {
            composable(Screen.PostsScreen.route) {
                PostsScreen(
                    onItemClicked = {
                        navController.navigate(
                            Screen.PostDetailScreen.withArguments(
                                mapOf(Screen.PostDetailScreen.ID to it.toString())
                            )
                        )
                    }
                )
            }
            composable(Screen.PostDetailScreen.route) {
                PostDetailScreen(
                    requireNotNull(it.arguments?.getString(Screen.PostDetailScreen.ID)?.toLong()),
                    onNavigateBack = { navController.popBackStack() }
                )
            }
        }
    }
}