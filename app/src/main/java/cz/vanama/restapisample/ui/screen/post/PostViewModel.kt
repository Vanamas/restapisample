package cz.vanama.restapisample.ui.screen.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.domain.usecase.GetPostUseCase
import cz.vanama.restapisample.ui.screen.common.model.UiState
import cz.vanama.restapisample.ui.screen.common.model.toUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

/**
 * Post detail view model.
 *
 * @author Martin Vana
 */
class PostViewModel(
    private val postId: Long,
    private val getPostUseCase: GetPostUseCase
) : ViewModel() {

    private val postFlow = MutableStateFlow<UiState<Post>>(UiState.Loading)
    val state = postFlow.asStateFlow()

    init {
        reload()
    }

    private fun reload() {
        viewModelScope.launch {
            postFlow.value = UiState.Loading
            postFlow.value =
                getPostUseCase(GetPostUseCase.Params(postId)).toUiState(retryAction = { reload() })
        }
    }
}