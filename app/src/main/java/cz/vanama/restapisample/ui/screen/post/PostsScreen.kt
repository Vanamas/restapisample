package cz.vanama.restapisample.ui.screen.post

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import cz.vanama.restapisample.R
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.ui.screen.common.UiStateScreen
import cz.vanama.restapisample.ui.screen.common.model.UiState
import cz.vanama.restapisample.ui.theme.RestApiSampleTheme
import org.koin.androidx.compose.getViewModel
import java.util.*

/**
 * Screen with list of posts.
 *
 * @author Martin Vana
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PostsScreen(onItemClicked: (Long) -> Unit, viewModel: PostsViewModel = getViewModel()) {
    val state = viewModel.postsStateFlow.collectAsState()
    val pullRefreshState = rememberPullRefreshState(
        refreshing = state.value is UiState.Loading,
        onRefresh = { viewModel.reload(true) }
    )

    UiStateScreen(state.value) {
        Box(modifier = Modifier.fillMaxSize()) {
            Posts(
                posts = it,
                Modifier
                    .fillMaxSize()
                    .pullRefresh(pullRefreshState)
            ) { post ->
                onItemClicked(post.id)
            }
            PullRefreshIndicator(
                refreshing = state.value is UiState.Loading,
                state = pullRefreshState,
                modifier = Modifier.align(Alignment.TopCenter),
            )
        }
    }
}

@Composable
private fun Posts(
    posts: List<Post>,
    modifier: Modifier = Modifier,
    clickListener: (Post) -> Unit
) {
    LazyColumn(modifier) {
        items(items = posts) { post ->
            Post(item = post, clickListener = clickListener)
            Divider()
        }
    }
}

@Composable
private fun Post(item: Post, clickListener: (Post) -> Unit) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .background(MaterialTheme.colorScheme.surface)
        .padding(PaddingValues(dimensionResource(id = R.dimen.padding_base)))
        .clickable { clickListener(item) }
    ) {
        Text(
            text = item.title,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun PostItemsPreview() {
    val posts = listOf(Post(1L, "XXX"), Post(1L, "YYY"))
    RestApiSampleTheme {
        Surface {
            Posts(posts = posts) {}
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun PostItemsDarkPreview() {
    val posts = List(30) { Post(1L, UUID.randomUUID().toString()) }
    RestApiSampleTheme {
        Surface {
            Posts(posts = posts) {}
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun PostItemPreview() {
    RestApiSampleTheme {
        Post(Post(0L, "Post lalalalalalala")) {}
    }
}

@Preview(showBackground = true)
@Composable
private fun PostItemDarkPreview() {
    RestApiSampleTheme(darkTheme = true) {
        Surface {
            Post(Post(0L, "Post lalalalalalala")) {}
        }
    }
}