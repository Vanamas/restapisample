package cz.vanama.restapisample

import android.app.Application
import android.content.pm.ApplicationInfo
import cz.vanama.restapisample.data.source.local.db.AppDatabase.Companion.dbModule
import cz.vanama.restapisample.data.source.remote.client.ApiClient.networkModule
import cz.vanama.restapisample.di.repositoriesModule
import cz.vanama.restapisample.di.useCasesModule
import cz.vanama.restapisample.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber


/**
 * Application class of the whole app.
 *
 * @author Martin Vana
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        val isDebug = applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
        if (isDebug) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                networkModule,
                dbModule,
                useCasesModule,
                repositoriesModule,
                viewModelsModule
            )
        }
    }
}