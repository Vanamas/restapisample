package cz.vanama.restapisample.domain.usecase

import cz.vanama.restapisample.domain.common.usecases.UseCaseResult
import cz.vanama.restapisample.domain.common.usecases.UseCaseResultNoParams
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.domain.repository.PostRepository

/**
 * Use case for getting post by id.
 *
 * @author Martin Vana
 */
class GetPostUseCase(
    private val postRepository: PostRepository
) : UseCaseResult<Post, GetPostUseCase.Params>() {

    override suspend fun doWork(params: Params) = postRepository.getPost(params.id)

    data class Params(val id: Long)
}