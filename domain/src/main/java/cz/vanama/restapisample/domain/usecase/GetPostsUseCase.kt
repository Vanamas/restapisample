package cz.vanama.restapisample.domain.usecase

import cz.vanama.restapisample.domain.common.usecases.UseCaseResult
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.domain.repository.PostRepository

/**
 * Use case for getting all posts.
 *
 * @author Martin Vana
 */
class GetPostsUseCase(
    private val postRepository: PostRepository
) : UseCaseResult<List<Post>, GetPostsUseCase.Params>() {

    override suspend fun doWork(params: Params) = postRepository.getPosts(params.forceReload)

    data class Params(val forceReload: Boolean = false)
}