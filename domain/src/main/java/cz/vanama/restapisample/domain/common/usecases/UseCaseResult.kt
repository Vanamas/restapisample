package cz.vanama.restapisample.domain.common.usecases

import cz.vanama.restapisample.domain.common.Result

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents an execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 *
 * Use cases are the entry points to the domain layer.
 *
 * @author Martin Vana
 */
abstract class UseCaseResult<out T : Any, in Params> : UseCase<Result<T>, Params>()
