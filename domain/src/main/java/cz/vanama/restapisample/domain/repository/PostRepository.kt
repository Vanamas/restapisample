package cz.vanama.restapisample.domain.repository

import cz.vanama.restapisample.domain.common.Result
import cz.vanama.restapisample.domain.model.Post

/**
 * Post repository.
 *
 * @author Martin Vana
 */
interface PostRepository {

    suspend fun getPosts(forceReload: Boolean = false): Result<List<Post>>

    suspend fun getPost(id: Long): Result<Post>

    suspend fun createPost(post: Post): Result<Long>

    suspend fun updatePost(id: Long, post: Post): Result<Unit>

    suspend fun deletePost(id: Long): Result<Unit>
}