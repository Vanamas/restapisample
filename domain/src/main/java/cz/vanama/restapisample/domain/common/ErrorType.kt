package cz.vanama.restapisample.domain.common

/**
 * Enum with all possible errors.
 *
 * @author Martin Vana
 */
enum class ErrorType {
    RESPONSE_UNAUTHORIZED,
    RESPONSE_TIMEOUT,
    RESPONSE_NOT_FOUND,
    RESPONSE_INTERNAL_SERVER_ERROR,
    NOT_CONNECTED,
    UNKNOWN
}