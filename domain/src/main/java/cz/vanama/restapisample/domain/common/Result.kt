package cz.vanama.restapisample.domain.common

/**
 * A generic class that holds a value with its loading status.
 *
 * @param <T>
 *
 * @author Martin Vana
 */
sealed class Result<out T> {
    data class Success<T>(val data: T) : Result<T>()
    data class Error(val errorType: ErrorType) : Result<Nothing>()
    object Loading : Result<Nothing>()
}