package cz.vanama.restapisample.data.common

import cz.vanama.restapisample.domain.common.ErrorType
import cz.vanama.restapisample.domain.common.Result
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

class ResponseHandlerTest : StringSpec({

    val handler = ResponseHandler()

    "handleSuccess should return Result.Success with correct data" {
        val data = "Test data"
        val result = handler.handleSuccess(data)
        result shouldBe Result.Success(data)
    }

    "handleException should return Result.Error with correct error type for HttpException" {
        val httpException = HttpException(
            Response.error<String>(
                404,
                "".toResponseBody("application/txt".toMediaTypeOrNull())
            )
        )
        val result = handler.handleException(httpException)
        result shouldBe Result.Error(ErrorType.RESPONSE_NOT_FOUND)
    }

    "handleException should return Result.Error with RESPONSE_TIMEOUT error type for SocketTimeoutException" {
        val socketTimeoutException = SocketTimeoutException()
        val result = handler.handleException(socketTimeoutException)
        result shouldBe Result.Error(ErrorType.RESPONSE_TIMEOUT)
    }

    "handleException should return Result.Error with UNKNOWN error type for other exceptions" {
        val exception = Exception()
        val result = handler.handleException(exception)
        result shouldBe Result.Error(ErrorType.UNKNOWN)
    }
})
