package cz.vanama.restapisample.data.mapper

import cz.vanama.restapisample.data.model.PostDto
import cz.vanama.restapisample.data.model.PostEntity
import cz.vanama.restapisample.domain.model.Post
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class PostMapperExtensionsTest : StringSpec({

    "PostEntity to Post model mapping should be correct" {
        val entity = PostEntity(1, "Title", "Body", 1)
        val model = entity.toModel()

        model.id shouldBe entity.id
        model.title shouldBe entity.title
        model.body shouldBe entity.body
        model.userId shouldBe entity.userId
    }

    "PostDto to Post model mapping should be correct" {
        val dto = PostDto(1, "Title", "Body", 1)
        val model = dto.toModel()

        model.id shouldBe dto.id
        model.title shouldBe dto.title
        model.body shouldBe dto.body
        model.userId shouldBe dto.userId
    }

    "Post to PostDto mapping should be correct" {
        val model = Post(1, "Title", "Body", 1)
        val dto = model.toDto()

        dto.id shouldBe model.id
        dto.title shouldBe model.title
        dto.body shouldBe model.body
        dto.userId shouldBe model.userId
    }

    "Post to PostEntity mapping should be correct" {
        val model = Post(1, "Title", "Body", 1)
        val entity = model.toEntity()

        entity.id shouldBe model.id
        entity.title shouldBe model.title
        entity.body shouldBe model.body
        entity.userId shouldBe model.userId
    }
})
