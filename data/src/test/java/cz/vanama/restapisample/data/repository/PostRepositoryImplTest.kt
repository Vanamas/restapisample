package cz.vanama.restapisample.data.repository

import cz.vanama.restapisample.data.common.ResponseHandler
import cz.vanama.restapisample.data.mapper.toDto
import cz.vanama.restapisample.data.mapper.toModel
import cz.vanama.restapisample.data.model.PostEntity
import cz.vanama.restapisample.data.source.local.db.dao.PostDao
import cz.vanama.restapisample.data.source.remote.PostsApiSource
import cz.vanama.restapisample.data.usecase.CacheTimeExpiredUseCase
import cz.vanama.restapisample.data.usecase.IsDeviceOnlineUseCase
import cz.vanama.restapisample.domain.common.ErrorType
import cz.vanama.restapisample.domain.common.Result
import cz.vanama.restapisample.domain.model.Post
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest

class PostRepositoryImplTest : StringSpec({

    // Mock dependencies
    val postsApiSource: PostsApiSource = mockk()
    val postDao: PostDao = mockk()
    val responseHandler: ResponseHandler = mockk()
    val isDeviceOnlineUseCase: IsDeviceOnlineUseCase = mockk()
    val cacheTimeExpiredUseCase: CacheTimeExpiredUseCase = mockk()

    // Initialize repository with mocked dependencies
    val repository = PostRepositoryImpl(
        postsApiSource,
        postDao,
        responseHandler,
        isDeviceOnlineUseCase,
        cacheTimeExpiredUseCase
    )

    "getPost should return post when available in database" {
        // Mock the expected returned data
        val postEntity = PostEntity(1, "Title", "Body", 1)
        val postModel = postEntity.toModel()

        // Mock the PostDao getPost to return the expected data
        coEvery { postDao.getPost(1) } returns postEntity

        runTest {
            val result = repository.getPost(1)
            result shouldBe Result.Success(postModel)
        }
    }

    "getPost should return error when exception occurs" {
        // Mock the PostDao getPost to return null
        coEvery { postDao.getPost(1) } returns null

        // Mock the postsApiSource fetchPost to throw exception
        coEvery { postsApiSource.fetchPost(1) } throws Exception()

        // Mock the responseHandler to handle the exception and return an error
        coEvery { responseHandler.handleException(any()) } returns Result.Error(ErrorType.UNKNOWN)

        runTest {
            val result = repository.getPost(1)
            result shouldBe Result.Error(ErrorType.UNKNOWN)
        }
    }

    "createPost should return id when post is created successfully" {
        // Mock the expected returned data
        val postId: Long = 1
        val postModel = Post(id = postId, title = "Title", body = "Body", userId = 1)

        // Mock the postsApiSource createPost to return the expected data
        coEvery { postsApiSource.createPost(postModel.toDto()) } returns postId

        runTest {
            val result = repository.createPost(postModel)
            result shouldBe Result.Success(postId)
        }
    }

    "createPost should return error when exception occurs" {
        // Mock the expected returned data
        val postModel = Post(id = 1, title = "Title", body = "Body", userId = 1)

        // Mock the postsApiSource createPost to throw exception
        coEvery { postsApiSource.createPost(postModel.toDto()) } throws Exception()

        // Mock the responseHandler to handle the exception and return an error
        coEvery { responseHandler.handleException(any()) } returns Result.Error(ErrorType.UNKNOWN)

        runTest {
            val result = repository.createPost(postModel)
            result shouldBe Result.Error(ErrorType.UNKNOWN)
        }
    }

    "updatePost should return unit when post is updated successfully" {
        // Mock the expected returned data
        val postId: Long = 1
        val postModel = Post(id = postId, title = "Title", body = "Body", userId = 1)

        // Mock the postsApiSource updatePost to return Unit
        coEvery { postsApiSource.updatePost(postId, postModel.toDto()) } returns Unit

        runTest {
            val result = repository.updatePost(postId, postModel)
            result shouldBe Result.Success(Unit)
        }
    }

    "updatePost should return error when exception occurs" {
        // Mock the expected returned data
        val postId: Long = 1
        val postModel = Post(id = postId, title = "Title", body = "Body", userId = 1)

        // Mock the postsApiSource updatePost to throw exception
        coEvery { postsApiSource.updatePost(postId, postModel.toDto()) } throws Exception()

        // Mock the responseHandler to handle the exception and return an error
        coEvery { responseHandler.handleException(any()) } returns Result.Error(ErrorType.UNKNOWN)

        runTest {
            val result = repository.updatePost(postId, postModel)
            result shouldBe Result.Error(ErrorType.UNKNOWN)
        }
    }

})
