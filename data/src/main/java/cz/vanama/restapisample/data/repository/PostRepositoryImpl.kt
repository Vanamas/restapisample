package cz.vanama.restapisample.data.repository

import cz.vanama.restapisample.data.common.ResponseHandler
import cz.vanama.restapisample.data.mapper.toDto
import cz.vanama.restapisample.data.mapper.toEntity
import cz.vanama.restapisample.data.mapper.toModel
import cz.vanama.restapisample.data.source.local.db.dao.PostDao
import cz.vanama.restapisample.data.source.remote.PostsApiSource
import cz.vanama.restapisample.data.usecase.CacheTimeExpiredUseCase
import cz.vanama.restapisample.data.usecase.IsDeviceOnlineUseCase
import cz.vanama.restapisample.domain.common.ErrorType
import cz.vanama.restapisample.domain.common.Result
import cz.vanama.restapisample.domain.model.Post
import cz.vanama.restapisample.domain.repository.PostRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.util.*

/**
 * Post repository implementation.
 *
 * @author Martin Vana
 */
class PostRepositoryImpl(
    private val postsApiSource: PostsApiSource,
    private val postDao: PostDao,
    private val responseHandler: ResponseHandler,
    private val isDeviceOnlineUseCase: IsDeviceOnlineUseCase,
    private val cacheTimeExpiredUseCase: CacheTimeExpiredUseCase
) : PostRepository {

    private var lastUpdate: Date? = null

    override suspend fun getPosts(forceReload: Boolean): Result<List<Post>> {
        return withContext(IO) {
            if (isDeviceOnlineUseCase()) {
                // If device is online and cache has been expired (or never created) we want to
                // fetch list from backend and create cache.
                if (forceReload || cacheTimeExpiredUseCase(CacheTimeExpiredUseCase.Params(lastUpdate))) {
                    try {
                        val data = postsApiSource.fetchPosts().map { it.toModel() }
                        // Delete all posts and insert new
                        postDao.deleteAllPosts()
                        postDao.insert(*data.map { it.toEntity() }.toTypedArray())
                        // Send successful result.
                        responseHandler.handleSuccess(data)
                    } catch (e: Exception) {
                        responseHandler.handleException(e)
                    }
                } else {
                    // Get cached data from DB.
                    Result.Success(postDao.getPosts().map { it.toModel() })
                }
            } else if (forceReload) { // Force reload but no connection.
                Result.Error(ErrorType.NOT_CONNECTED)
            } else {
                // If device is offline we show cached data. If there is no data we send error.
                with(postDao.getPosts()) {
                    if (isNotEmpty()) {
                        Result.Success(this.map { it.toModel() })
                    } else {
                        Result.Error(ErrorType.NOT_CONNECTED)
                    }
                }
            }
        }
    }

    override suspend fun getPost(id: Long): Result<Post> =
        // First we look to DB because the post should be already stored.
        postDao.getPost(id)?.let {
            Result.Success(it.toModel())
        } ?: try {
            Result.Success(postsApiSource.fetchPost(id).toModel())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }

    override suspend fun createPost(post: Post): Result<Long> =
        try {
            Result.Success(postsApiSource.createPost(post.toDto()))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }

    override suspend fun updatePost(id: Long, post: Post): Result<Unit> =
        try {
            Result.Success(postsApiSource.updatePost(post.id, post.toDto()))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }

    override suspend fun deletePost(id: Long): Result<Unit> =
        try {
            Result.Success(postsApiSource.deletePost(id))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
}