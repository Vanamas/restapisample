package cz.vanama.restapisample.data.common

import cz.vanama.restapisample.domain.common.ErrorType
import cz.vanama.restapisample.domain.common.Result
import retrofit2.HttpException
import timber.log.Timber
import java.net.SocketTimeoutException

/**
 * Handler for http responses.
 *
 * @author Martin Vana
 */
class ResponseHandler {

    fun <T : Any> handleSuccess(data: T): Result<T> {
        return Result.Success(data)
    }

    fun handleException(e: Exception): Result<Nothing> {
        Timber.w(e)
        return when (e) {
            is HttpException -> Result.Error(getError(e.code()))
            is SocketTimeoutException -> Result.Error(ErrorType.RESPONSE_TIMEOUT)
            else -> Result.Error(getError(Int.MAX_VALUE))
        }
    }

    private fun getError(code: Int): ErrorType {
        return when (code) {
            401 -> ErrorType.RESPONSE_UNAUTHORIZED
            404 -> ErrorType.RESPONSE_NOT_FOUND
            500 -> ErrorType.RESPONSE_INTERNAL_SERVER_ERROR
            else -> ErrorType.UNKNOWN
        }
    }
}