package cz.vanama.restapisample.data.source.local.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cz.vanama.restapisample.data.model.PostEntity
import cz.vanama.restapisample.data.source.local.db.dao.PostDao
import org.koin.dsl.module

/**
 * Application database.
 *
 * @author Martin Vana
 */
@Database(entities = [PostEntity::class], version = AppDatabase.VERSION)
abstract class AppDatabase : RoomDatabase() {

    abstract val postDao: PostDao

    companion object {
        private const val NAME = "appDb"
        const val VERSION = 2

        val dbModule = module {
            single { provideDatabase(get()) }
            single { providePostDao(get()) }
        }

        private fun provideDatabase(application: Application): AppDatabase =
            Room.databaseBuilder(application, AppDatabase::class.java, NAME)
                .fallbackToDestructiveMigration().build()

        private fun providePostDao(database: AppDatabase) = database.postDao
    }
}