package cz.vanama.restapisample.data.source.remote.client

import com.squareup.moshi.Moshi
import cz.vanama.restapisample.data.source.remote.PostsApiSource
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


/**
 * Api source - retrofit configuration.
 *
 * @author Martin Vana
 */
object ApiClient {

    val networkModule = module {
        factory { provideOkHttpClient() }
        factory { providePostApi(get()) }
        single { provideRetrofit(get()) }
    }

    private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val moshi = Moshi.Builder().build()
        return Retrofit.Builder()
            .baseUrl(API_URL).client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient().newBuilder()
            .addInterceptor(interceptor)
            .build()
    }

    private fun providePostApi(retrofit: Retrofit): PostsApiSource =
        retrofit.create(PostsApiSource::class.java)

    private const val API_URL = "https://jsonplaceholder.typicode.com/"
}