package cz.vanama.restapisample.data.source.remote

import cz.vanama.restapisample.data.model.PostDto
import retrofit2.http.*

/**
 * Sample post API source.
 *
 * @author Martin Vana
 */
interface PostsApiSource {

    @GET("posts")
    suspend fun fetchPosts(): List<PostDto>

    @GET("posts/{id}")
    suspend fun fetchPost(@Path("id") id: Long): PostDto

    @GET("posts/{id}")
    suspend fun createPost(@Body postDto: PostDto): Long

    @PUT("posts/{id}")
    suspend fun updatePost(@Path("id") id: Long, @Body postDto: PostDto)

    @DELETE("posts/{id}")
    suspend fun deletePost(@Path("id") id: Long)
}