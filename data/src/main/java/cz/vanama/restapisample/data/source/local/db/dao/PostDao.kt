package cz.vanama.restapisample.data.source.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.vanama.restapisample.data.model.PostEntity

/**
 * Post DAO.
 *
 * @author Martin Vana
 */
@Dao
interface PostDao {

    @Query("SELECT * FROM posts")
    suspend fun getPosts(): List<PostEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg postEntity: PostEntity)

    @Query("DELETE FROM posts")
    suspend fun deleteAllPosts()

    @Query("SELECT * FROM posts WHERE id = :id")
    suspend fun getPost(id: Long): PostEntity?
}