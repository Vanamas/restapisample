package cz.vanama.restapisample.data.mapper

import cz.vanama.restapisample.data.model.PostDto
import cz.vanama.restapisample.data.model.PostEntity
import cz.vanama.restapisample.domain.model.Post

/**
 * Mapper extensions of [PostDto], [Post] and [PostEntity] models.
 *
 * @author Martin Vana
 */

fun PostEntity.toModel() = Post(id, title, body, userId)

fun PostDto.toModel() = Post(id, title, body, userId)

fun Post.toDto() = PostDto(id, title, body, userId)

fun Post.toEntity() = PostEntity(id, title, body, userId)