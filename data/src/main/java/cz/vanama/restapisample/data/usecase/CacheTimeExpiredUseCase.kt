package cz.vanama.restapisample.data.usecase

import cz.vanama.restapisample.domain.common.usecases.UseCase
import cz.vanama.restapisample.domain.common.usecases.UseCaseNoParams
import java.net.InetAddress
import java.util.*


/**
 * Checks if device is connected to internet.
 *
 * @author Martin Vana
 */
class CacheTimeExpiredUseCase : UseCase<Boolean, CacheTimeExpiredUseCase.Params>() {

    companion object {
        // Cache time in milliseconds (2 minutes)
        private const val CACHE_TIME = 2 * 60 * 1000
    }

    override suspend fun doWork(params: Params) = params.lastUpdate == null
            || Date().time > params.lastUpdate.time + CACHE_TIME

    data class Params(val lastUpdate: Date?)
}