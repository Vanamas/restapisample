package cz.vanama.restapisample.data.usecase

import cz.vanama.restapisample.domain.common.usecases.UseCaseNoParams
import java.net.InetAddress


/**
 * Checks if device is connected to internet.
 *
 * @author Martin Vana
 */
class IsDeviceOnlineUseCase : UseCaseNoParams<Boolean>() {

    override suspend fun doWork(params: Unit) = isInternetAvailable()

    private fun isInternetAvailable() = runCatching {
        InetAddress.getByName(TEST_DOMAIN)
    }.fold(
        onSuccess = { !it.equals("") },
        onFailure = { false }
    )

    companion object {
        const val TEST_DOMAIN = "google.com"
    }
}