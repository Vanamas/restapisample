package cz.vanama.restapisample.data.model

import com.squareup.moshi.Json

data class PostDto(

    @Json(name = "id")
    val id: Long,

    @Json(name = "title")
    val title: String,

    @Json(name = "body")
    val body: String? = null,

    @Json(name = "userId")
    val userId: Int? = null
)
